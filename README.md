[![QGIS Version](https://img.shields.io/badge/QGIS-3.x-green)](https://qgis.org)[![QGIS.org](https://img.shields.io/badge/QGIS.org-published-green)](https://plugins.qgis.org/plugins/OsmAnd_bridge/#plugin-versions)

# OSMAnd bridge

Import tracks, favorites, itinerary and AV notes from OsmAnd
