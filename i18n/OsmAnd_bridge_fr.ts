<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name></name>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="70"/>
        <source>Audio notes</source>
        <translation>Notes audio</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="77"/>
        <source>Video notes</source>
        <translation>Notes vidéo</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="84"/>
        <source>Picture notes</source>
        <translation>Notes photo</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="128"/>
        <source>Audiovisual notes</source>
        <translation>Notes audiovisuelles</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="156"/>
        <source>Open file</source>
        <translation>Ouvrir le fichier</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="188"/>
        <source>Waypoints</source>
        <translation>Waypoints</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="188"/>
        <source>Routes</source>
        <translation>Routes</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="188"/>
        <source>Tracks</source>
        <translation>Traces</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="189"/>
        <source>Route points</source>
        <translation>Points de route</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="189"/>
        <source>Track points</source>
        <translation>Points de trace</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="217"/>
        <source>favorites</source>
        <translation>favoris</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_process.py" line="219"/>
        <source>Itinerary</source>
        <translation>Itineraires</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_settings_management.py" line="44"/>
        <source>Don&apos;t show this message again</source>
        <translation>Ne plus afficher ce message</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="17"/>
        <source>OsmAnd bridge - Select items to import </source>
        <translation>OsmAnd bridge - Sélectionnez les éléments à importer</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="123"/>
        <source>Favorites</source>
        <translation type="unfinished">Favoris</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="145"/>
        <source>Itinerary</source>
        <translation type="unfinished">Itineraire</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="164"/>
        <source>AVnotes</source>
        <translation type="unfinished">Notes audiovisuelles</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="177"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select the &apos;file&apos; directory on you computer:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="193"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select the track(s) you want to download:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="206"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select the point feature(s) you want to download:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="219"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Choose the destination folder on your computer:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="251"/>
        <source>Device</source>
        <translation type="unfinished">Appareil</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="267"/>
        <source>Local directory</source>
        <translation type="unfinished">Répertoire local</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="293"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Import &lt;/span&gt;&lt;a href=&quot;https://osmand.net/&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline; color:#1d99f3;&quot;&gt;OsmAnd&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt; data from:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="309"/>
        <source>Select device</source>
        <translation type="unfinished">Sélectionnez l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="325"/>
        <source>Refresh device list</source>
        <translation type="unfinished">Rafraîchir la liste des appareils</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.ui" line="341"/>
        <source>Clear selection</source>
        <translation>Effacer la sélection</translation>
    </message>
</context>
<context>
    <name>OsmAndBridge</name>
    <message>
        <location filename="../OsmAnd_bridge.py" line="215"/>
        <source>&amp;OsmAnd bridge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="203"/>
        <source>Import tracks, favorites, itinerary and AV notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="276"/>
        <source>Issue when trying to create destination geopackage file ({self.dest_gpkg})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="292"/>
        <source>Importing favorites ({file})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="299"/>
        <source>Something went wrong while importing favorites ({file})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="309"/>
        <source>Importing itinerary ({file})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="316"/>
        <source>Something went wrong while importing itinerary ({file})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="329"/>
        <source>Importing track files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="352"/>
        <source>Something went wrong while importing {currentQTableWidgetItem.text()}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="365"/>
        <source>Map background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="367"/>
        <source>No internet connection. Unable to load OSM tile background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="385"/>
        <source>♪♪ This is the End, my only friend, the End ♪♪</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="226"/>
        <source>hide_unstable_warning_message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge.py" line="228"/>
        <source>This plugin uses libraries known to be unstable to access devices (MTP protocol). 
In rare cases, it can cause Qgis to crash.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OsmAndBridgeImportDialog</name>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="99"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="99"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="99"/>
        <source>Last Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="341"/>
        <source>Check that it is properly connected and unlocked.
 Try unplugging and replugging it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="351"/>
        <source>Check that your device is properly connected and unlocked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="488"/>
        <source>not valid output file path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="502"/>
        <source>*Not a valid directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="508"/>
        <source>no valid OsmAnd tracks path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="520"/>
        <source>no gpx file to import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="525"/>
        <source>found favorites.gpx.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="530"/>
        <source>No favorites found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="537"/>
        <source>found ./itinerary.gpx.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="542"/>
        <source>./itinerary.gpx not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="549"/>
        <source>no valid OsmAnd avnotes path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="561"/>
        <source>no avnote file to import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="351"/>
        <source>No device found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="341"/>
        <source>Can&apos;t connect to device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="222"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="225"/>
        <source>, especially under GNU/Linux and macOS:(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="227"/>
        <source>Be patient! 
This operation can take several minutes{mtpy_msg}.
In rare cases, it can cause Qgis to crash.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="300"/>
        <source>No files found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="301"/>
        <source>OSMAnd files could not be found on {device_model_name}. Try copying the files to your hard disk and importing them into QGIS from the local directory.</source>
        <translation>Les fichiers d&apos;OSMAnd n&apos;ont pas pu être trouvés sur {device}. Essayez de copier les fichiers sur votre disque dur et importez-les dans QGIS depuis le répertoire local.</translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="434"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select the OSMand &apos;file&apos; directory on you computer:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../OsmAnd_bridge_import_dialog.py" line="443"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select your device:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
